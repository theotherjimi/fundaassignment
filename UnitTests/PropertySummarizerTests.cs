﻿using System.Linq;
using System.Threading.Tasks;
using FundaAssignment;
using FundaAssignment.Models;
using FundaAssignment.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTests
{
    [TestClass]
    public class PropertySummarizerTests
    {
        [TestMethod]
        public async Task GetSummaryTest()
        {
            // arrange
            var mockPropertyRepository = new Mock<IPropertyRepository>();
            var mockFundaResponse = new FundaResponse
            {
                Paging = new Paging
                {
                    AantalPaginas = 1,
                    HuidigePagina = 1
                },
                Objects = new[]
                {
                    new FundaObject
                    {
                        MakelaarId = 1,
                        MakelaarNaam = "Test 1"
                    },
                    new FundaObject
                    {
                        MakelaarId = 2,
                        MakelaarNaam = "Test 2"
                    },
                    new FundaObject
                    {
                        MakelaarId = 3,
                        MakelaarNaam = "Test 3"
                    },
                    new FundaObject
                    {
                        MakelaarId = 4,
                        MakelaarNaam = "Test 4"
                    },
                    new FundaObject
                    {
                        MakelaarId = 5,
                        MakelaarNaam = "Test 5"
                    },
                    new FundaObject
                    {
                        MakelaarId = 5,
                        MakelaarNaam = "Test 5"
                    },
                    new FundaObject
                    {
                        MakelaarId = 5,
                        MakelaarNaam = "Test 5"
                    },
                }
            };
            mockPropertyRepository
                .Setup(r => r.Search("/fakequery/", 1))
                .ReturnsAsync(mockFundaResponse);
            var summarizer = new PropertySummarizer(mockPropertyRepository.Object);

            // act
            var result = await summarizer.GetSummary("/fakequery/");

            // assert
            var agentResults = result.ToList();
            Assert.AreEqual(5, agentResults.Count);
            Assert.AreEqual("Test 5", agentResults.First().AgentName);
        }
    }
}