﻿namespace FundaAssignment.Models
{
    public class AgentResult
    {
        public string AgentName { get; set; }
        public int PropertyCount { get; set; }

        public AgentResult(string agentName, int propertyCount)
        {
            AgentName = agentName;
            PropertyCount = propertyCount;
        }
    }
}
