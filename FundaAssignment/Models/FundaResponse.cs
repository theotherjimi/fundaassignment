﻿namespace FundaAssignment.Models
{
    public class FundaResponse
    {
        public Metadata Metadata { get; set; }
        public FundaObject[] Objects { get; set; }
        public Paging Paging { get; set; }
        // Total Objects
        public int TotaalAantalObjecten { get; set; }
    }

    public class Metadata
    {
        public string ObjectType { get; set; }
        // Definition ??
        public string Omschrijving { get; set; }
        // Title
        public string Titel { get; set; }
    }

    public class Paging
    {
        // Total Pages
        public int AantalPaginas { get; set; }
        // Current Page
        public int HuidigePagina { get; set; }
        // Next URL
        public string VolgendeUrl { get; set; }
        // Previous URL
        public string VorigeUrl { get; set; }
    }

    public class FundaObject
    {
        public int MakelaarId { get; set; }
        public string MakelaarNaam { get; set; }
    }
}
