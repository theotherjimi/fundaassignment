﻿using FundaAssignment.Models;

namespace FundaAssignment.Repositories
{
    public interface IPropertyRepository
    {
        // A correct implementation should probably have a implementation independent model returned from this method
        public Task<FundaResponse> Search(string query, int page);
    }
}