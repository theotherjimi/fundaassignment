﻿namespace FundaAssignment.Repositories.Configuration;

public class ApiOptions
{
    public string ApiKey { get; set; }
}