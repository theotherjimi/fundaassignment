﻿using System.Net.Http.Json;
using FundaAssignment.Models;
using FundaAssignment.Repositories.Configuration;
using Microsoft.Extensions.Options;

namespace FundaAssignment.Repositories
{
    public class FundaRepository : IPropertyRepository
    {
        private readonly IOptions<ApiOptions> _apiOptions;
        private readonly HttpClient _client;

        public FundaRepository(IHttpClientFactory httpClientFactory, IOptions<ApiOptions> apiOptions)
        {
            _apiOptions = apiOptions;
            _client = httpClientFactory.CreateClient("partnerapi.funda.nl");
        }

        public async Task<FundaResponse> Search(string query, int page)
        {
            var response = await _client.GetAsync($"/feeds/Aanbod.svc/json/{_apiOptions.Value.ApiKey}/?type=koop&zo={query}&page={page}&pagesize=25");
            if (response.IsSuccessStatusCode)
            {
                var fundaResponse = await response.Content.ReadFromJsonAsync<FundaResponse>();
                if (fundaResponse == null)
                {
                    throw new Exception("Cannot deserialize response");
                }
                return fundaResponse;
            }
            throw new Exception($"Could not retrieve property info. status: {response.StatusCode}");
        }
    }
}
