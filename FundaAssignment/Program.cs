﻿using FundaAssignment.Models;
using FundaAssignment.Repositories;
using FundaAssignment.Repositories.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using Polly.Contrib.WaitAndRetry;
using Polly.Extensions.Http;
using Polly.RateLimit;
using Polly.Timeout;

namespace FundaAssignment
{
    public static class Program
    {
        public static async Task Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);
            await services
                .AddSingleton<Executor, Executor>()
                .BuildServiceProvider()
                .GetService<Executor>()
                !.Execute();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            // configure the API Key - In a production application this would be sourced from a config store somewhere
            services.AddOptions<ApiOptions>().Configure(x => x.ApiKey = "ac1b0b1572524640a0ecc54de453ea9f");
            // retry failed request 5 times with exponential backoff
            var retryPolicy = HttpPolicyExtensions
                .HandleTransientHttpError()
                .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.Unauthorized) // 401 returned when rate limit hit
                .Or<TimeoutRejectedException>()
                .Or<RateLimitRejectedException>()
                .WaitAndRetryAsync(Backoff.DecorrelatedJitterBackoffV2(TimeSpan.FromSeconds(10), 5));

            // requests timeout after 10 seconds
            var timeoutPolicy = Policy.TimeoutAsync<HttpResponseMessage>(10);

            // rate limit requests to 100 per minute
            var rateLimitPolicy = Policy
                .RateLimitAsync<HttpResponseMessage>(95, TimeSpan.FromMinutes(1), 95);
            services.AddHttpClient("partnerapi.funda.nl", c => c.BaseAddress = new Uri("https://partnerapi.funda.nl"))
                .AddPolicyHandler(retryPolicy)
                .AddPolicyHandler(rateLimitPolicy)
                .AddPolicyHandler(timeoutPolicy);

            services.AddSingleton<PropertySummarizer, PropertySummarizer>();
            services.AddSingleton<IPropertyRepository, FundaRepository>();
        }
    }

    internal class Executor
    {
        private readonly PropertySummarizer _propertySummarizer;

        public Executor(PropertySummarizer propertySummarizer)
        {
            _propertySummarizer = propertySummarizer;
        }

        internal async Task Execute()
        {
            try
            {
                PrintResult(await _propertySummarizer.GetSummary("/amsterdam/"));
                PrintResult(await _propertySummarizer.GetSummary("/amsterdam/tuin/"));
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
                throw;
            }
        }

        private static void PrintResult(IEnumerable<AgentResult> topTen)
        {
            Console.WriteLine("|------------------------------------------|-------|");
            Console.WriteLine("| {0,-40} | {1,5} |", "Agent", "Count");
            Console.WriteLine("|------------------------------------------|-------|");
            foreach (var result in topTen)
            {
                Console.WriteLine("| {0,-40} | {1,5:N0} |", result.AgentName, result.PropertyCount);
            }
            Console.WriteLine("|------------------------------------------|-------|");
            Console.WriteLine();
        }
    }
}