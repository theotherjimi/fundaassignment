﻿using FundaAssignment.Models;
using FundaAssignment.Repositories;

namespace FundaAssignment
{
    public class PropertySummarizer
    {
        private readonly IPropertyRepository _propertyRepository;

        public PropertySummarizer(IPropertyRepository propertyRepository)
        {
            _propertyRepository = propertyRepository;
        }

        public async Task<IEnumerable<AgentResult>> GetSummary(string propertyQuery)
        {
            var result = await GetAllObjectsForQuery(propertyQuery);
            return result
                .GroupBy(r => r.MakelaarId)
                .Select(r => new AgentResult(r.First().MakelaarNaam, r.Count()))
                .OrderByDescending(r => r.PropertyCount)
                .Take(10);
        }

        private async Task<IEnumerable<FundaObject>> GetAllObjectsForQuery(string query)
        {
            // make first call to find number of pages
            var firstPage = await _propertyRepository.Search(query, 1);
            var totalPages = firstPage.Paging.AantalPaginas;
            var tasks = new List<Task<FundaResponse>>();
            // then get the rest of the pages in parallel
            for (var page = 2; page <= totalPages; page++)
            {
                tasks.Add(_propertyRepository.Search(query, page));
            }
            var results = await Task.WhenAll(tasks);
            var objects = results.SelectMany(result => result.Objects).ToList();
            objects.AddRange(firstPage.Objects);
            return objects;
        }
    }
}
