# Overview

I've chosen to implement the assignment in a simple console application using dotnet 6. This was largely chosen for simplicity

There are several important parts to this project.

1. The API is rate limited - I've chosen to use a library called Polly to handle errors and timeouts that may occur due to the API enforcing rate limits. In addition Polly supports client side rate-limiting, which while it may miss some scenarios (such if the application is restarted) is able to prevent most server-side rate-limiting from occurring. In any case if any issue happens during a http call it will be retried up to 5 times before printing an error and exiting.

2. Testability - There is a single unit test that demonstrates the testability of the solution. But in general projects are testable because components rely on abstractions rather than implementations. This is achieved in this project through the use of interfaces which define boundaries for different areas of concern and the use of dependency injection.

3. I've chosen to parallelize the network calls as much as possible - this provides a significant speedup to the applications runtime.